<?php

namespace Initial\Acl\Console\Commands;

use Illuminate\Console\Command;
use Initial\Acl\Services\SeedService;

class AclSeed extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'acl:seed';
    
    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Seed permissions';
    
    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        new SeedService($this, config('acl.paths'));
    }
}
