<?php

namespace Initial\Acl\Services;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Initial\Acl\Models\Permission;
use Initial\Acl\Models\Role;

class SeedService
{
    /**
    * Filesystem Instance
    *
    * @var Filesystem
    */
    protected $fs;
    
    /**
    * Command Instance
    * 
    * @var Command
    */
    protected $console;
    
    /**
    * Permissions files
    * 
    * @var array $files
    */
    protected $files = [];
    
    /**
    * @var $paths Permission folder paths
    * @var $console Console Instance
    */
    public function __construct(Command $console, $paths = null)
    {
        $this->console = $console;
        if (empty($paths)) {
            $this->console->error('Config : Empty paths permissions');
            die();
        }
        
        $this->fs = new Filesystem();
        foreach($paths as $path) {
            $this->files = array_merge($this->files, $this->fs->files($path));
        }
        
        $this->readFiles();
    }
    
    /**
    * Load Content of permissions files
    */
    public function readFiles()
    {
        foreach($this->files as $file) {
            $permissions = $this->fs->getRequire($file->getRealPath());
            $this->readPermissions($permissions);
        }
    }
    
    /**
    * @var array $permissions Array of permissions
    */
    public function readPermissions(array $permissions)
    {
        $admin = Role::where('name', 'admin')->firstOrFail();
        
        foreach($permissions as $group => $value) {
            foreach($value as $permission => $label) {
                $name = $group .'.'. $permission;
                if(! $p = Permission::where('name',$name)->first()) {
                    $p = $this->createPermission($name, $label);
                } else {
                    $this->console->info($name . ' already exist');
                }
                if(! $admin->hasPermission($p->name)) {
                    $admin->givePermissionTo($p);
                }
            }
        }
    }
    
    /**
    * Create permission
    * 
    * @var $name
    * @var $label
    */
    protected function createPermission(string $name,string $label): Permission
    {
        $p = new Permission;
        $p->name = $name;
        $p->label = $label;
        $p->save();
        
        return $p;
    }
}
