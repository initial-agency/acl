<?php

namespace Initial\Acl\Traits;

trait Role
{
    public function roles()
    {
        return $this->belongsToMany(\Initial\Acl\Models\Role::class);
    }

    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !!$role->intersect($this->roles)->count();
    }

    public function assignRole($role)
    {
        return $this->roles()->save(
            \Initial\Acl\Models\Role::whereName($role)->firstOrFail()
        );
    }

}
