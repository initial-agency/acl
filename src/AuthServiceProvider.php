<?php

namespace Initial\Acl;

use Initial\Acl\Models\Permission;
use Initial\Acl\Console\Commands\AclSeed;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/acl.php', 'acl'
        );
    }

    /**
     * Bootstrap the application events.
     *
     * @param GateContract $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations/');
        //Publish config file
        $this->publishes([
            __DIR__ . '/config/acl.php' => config_path('acl.php')
        ], 'config');

        if ($this->app->runningInConsole()) {
            $this->commands([
                AclSeed::class
            ]);
        }

        $this->registerPolicies();

        foreach ($this->getPermissions() as $permission) {
            $gate->define($permission->name, function ($user) use ($permission) {
                return $user->hasRole($permission->roles);
            });
        }
    }

    protected function getPermissions()
    {
        if (\App::runningInConsole()) {
            return [];
        }

        return Permission::with('roles')->get();
    }

}
