<?php

namespace Initial\Acl\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * Get all permissions for a role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * Give a permission to a role
     *
     * @param Permission $permission
     * @return Model
     */
    public function givePermissionTo(Permission $permission)
    {
        return $this->permissions()->save($permission);
    }

    public function hasPermission($permission)
    {
        if (is_string($permission)) {
            return $this->permissions->contains('name', $permission);
        }

        return !!$role->intersect($this->permissions)->count();
    }
}
